/*
 * Copyright (c) 2017, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 * WSO2 Inc. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package barley.lcm.core.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wso2.carbon.datasource.core.api.DataSourceService;
import org.wso2.carbon.kernel.configprovider.ConfigProvider;

import barley.lcm.core.util.LifecycleUtils;
import barley.lcm.sql.config.LifecycleConfigBuilder;
import barley.lcm.sql.exception.LifecycleManagerDatabaseException;
import barley.lcm.sql.utils.LifecycleMgtDBUtil;

public class LifecycleServiceComponent   {

    private static Logger  log = LoggerFactory.getLogger(LifecycleServiceComponent.class);
    private ConfigProvider configProvider;

    public void start() {
        LifecycleUtils.initiateLCMap();
        if (log.isDebugEnabled()) {
            log.debug("Lifecycle service is activated.");
        }
    }

    protected void onDataSourceServiceReady(DataSourceService service) {
        //this is required to enforce a dependency on datasources
    }

    /* (임시주석)
    protected void onJNDIReady(JNDIContextManager service) {
        try {
            LifecycleConfigBuilder.build(configProvider);
            LifecycleMgtDBUtil.initialize();


        } catch (LifecycleManagerDatabaseException e) {
            log.error("Failed to initialize database. " + e);
        }
    }

    protected void onJNDIUnregister(JNDIContextManager jndiContextManager) {
        log.debug("Registering lifecycle data source");
    }
    */

    protected void unregisterDataSourceService(DataSourceService dataSourceService) {
        log.debug("Un registering lifecycle data source");
    }

    protected void registerConfigProvider(ConfigProvider configProvider) {
        this.configProvider = configProvider;
    }

    /**
     * This is the unbind method, which gets called for ConfigProvider instance un-registrations.
     *
     * @param configProvider the ConfigProvider service that get unregistered.
     */
    protected void unregisterConfigProvider(ConfigProvider configProvider) {
        this.configProvider = null;
    }

}
